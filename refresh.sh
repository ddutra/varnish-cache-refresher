#!/bin/bash
#Send a PURGE Request to the URL followed by a GET request
URL="$1"

curl -X PURGE http://$URL/sitemap.xml
wget --quiet http://$URL/sitemap.xml --no-cache --output-document - | egrep -o "http(s?)://$URL[^ \"\'()\<>]+" | while read line; do
    if [[ $line == *.xml ]]
    then
        newURL=$line
        curl -X PURGE $newURL
        wget --quiet $newURL --no-cache --output-document - | egrep -o "http(s?)://$URL[^ \"\'()\<>]+" | while read newline; do
            if [[ $newline != *wp-content* ]]
            then
                curl -X PURGE $newline > /dev/null 2>&1
                curl -I $newline > /dev/null 2>&1
                echo $newline
            fi
        done
    else
        if [[ $line != *wp-content* ]]
        then        
            curl -X PURGE $line > /dev/null 2>&1
            curl -I $line > /dev/null 2>&1
            echo $line
        fi
    fi
done
