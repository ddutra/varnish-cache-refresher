# varnish-cache-refresher
This is a bash script that uses wget and curl to purge or warm up Varnish by targeting the sitemap.xml of the website

Requirements
------------
It uses wget and curl packages and the website should have a sitemap.xml to work.

Credits
------------
Original project by sys0dm1n
https://github.com/sys0dm1n/varnish-cache-warmer